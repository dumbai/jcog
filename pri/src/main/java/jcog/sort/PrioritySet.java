package jcog.sort;

import jcog.data.bit.MetalBitSet;
import jcog.data.list.Lst;
import jcog.data.map.ObjIntHashMap;
import org.eclipse.collections.api.block.function.primitive.DoubleToDoubleFunction;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.random.RandomGenerator;

public class PrioritySet<X> implements AutoCloseable {
    private final ObjIntHashMap<X> itemToIndex;
    private final Object[] items;
    private final double[] pri;

    /** used indices
     * */
    private final MetalBitSet used;

    private int size;
    private final int capacity;
    private double priTotal;
    private int priMinIndex;

    public PrioritySet(int capacity) {
        if (capacity <= 0)
            throw new IllegalArgumentException("Capacity must be greater than 0");

        this.capacity = capacity;
        this.itemToIndex = new ObjIntHashMap<>(capacity);
        this.items = new Object[capacity];
        this.pri = new double[capacity];
        this.used = MetalBitSet.bits(capacity);
        clear();
    }

    public void clear() {
        if (size > 0)
            clearItems();
        priTotal = 0;
        priMinIndex = -1;
    }

    private void clearItems() {
        Arrays.fill(items, 0, size, null);
        Arrays.fill(pri, 0, size, 0);
        itemToIndex.clear();
        used.clear();
        size = 0;
    }

    public double pri(X item) {
        int index = itemToIndex.getIfAbsent(item, -1);
        return index == -1 ? Double.NaN : pri[index];
    }

    public boolean put(X item, double priority) {
        validPri(priority);
        if (item == null) throw new NullPointerException();
        
        int existing = itemToIndex.getIfAbsent(item, -1);
        if (existing!=-1) {
            pri(existing, priority);
            return true;
        } else
            return insert(item, priority);
    }

    public void forEach(Consumer<X> x) {
        int c = capacity;
        var ii = items;
        for (int i = used.next(true, 0, c); i >= 0; i = used.next(true, i + 1, c))
            x.accept((X) ii[i]);
    }


    private boolean insert(X item, double priority) {
        if (size >= capacity) {
            if (priority < lowestPriority())
                return false;
            removeLowestPriorityItem();
        }

        _insert(item, priority);
        return true;
    }

    private void _insert(X item, double priority) {
        int index = used.next(false, 0, capacity);
        items[index] = item;
        pri[index] = priority;
        itemToIndex.put(item, index);
        used.set(index);
        size++;
        priTotal += priority;
        updateLowestPriorityIndex(index);
    }


    public boolean remove(X item) {
        if (item == null) return false;
        int index = itemToIndex.removeKeyIfAbsent(item, -1);
        if (index == -1) return false;
        priTotal = Math.max(priTotal - pri[index], 0);
        pri[index] = 0;
        items[index] = null;
        used.clear(index);
        size--;
        if (index == priMinIndex) findNewLowestPriorityIndex();

        return true;
    }

    /** TODO optimize and avoid pri calls */
    public boolean priMul(X item, double f) {
        return pri(item, p -> p * f);
    }

    public boolean pri(X item, DoubleToDoubleFunction update) {
        int index = itemToIndex.getIfAbsent(item, -1);
        if (index != -1) {
            pri(index, update.valueOf(pri[index]));
            return true;
        } else {
            return false;
        }
    }

    public boolean pri(X item, double newPriority) {
        validPri(newPriority);

        int index = itemToIndex.getIfAbsent(item, -1);
        if (index != -1) {
            pri(index, newPriority);
            return true;
        } else {
            return false;
        }
    }

    private static void validPri(double newPriority) {
        if (newPriority <= 0)
            throw new IllegalArgumentException("Priority must be greater than 0");
    }

    private void pri(int index, double newPriority) {
        var delta = newPriority - pri[index];
        if (delta!=0) {
            priTotal += delta;
            pri[index] = newPriority;
            updateLowestPriorityIndex(index);
        }
    }

    /** roulette select (sample) */
    public int getIndex(RandomGenerator rng) {
        int s = size;
        if (s == 0) return -1;
        double randomValue = rng.nextFloat() * priTotal;
        double cumulativeProbability = 0;
        int last = -1;
        int c = capacity;
        for (int i = used.next(true, 0, c); i >= 0; i = used.next(true, i + 1, c)) {
            cumulativeProbability += pri[i];
            if (randomValue <= cumulativeProbability)
                return i;
            last = i;
        }
        return last;
    }

    public X get(RandomGenerator rng) {
        var i = getIndex(rng);
        return i < 0 ? null : item(i);
    }

    public X item(int i) {
        return (X) items[i];
    }

    private void removeLowestPriorityItem() {
        if (priMinIndex != -1)
            remove(lowestPriorityItem());
    }

    private X lowestPriorityItem() {
        return item(priMinIndex);
    }

    private void updateLowestPriorityIndex(int index) {
        if (priMinIndex == -1 || pri[index] < pri[priMinIndex])
            priMinIndex = index;
    }

    private double lowestPriority() {
        return pri[priMinIndex];
    }

    private void findNewLowestPriorityIndex() {
        priMinIndex = -1;
        double priMin = Double.POSITIVE_INFINITY;
        int c = capacity;
        for (int i = used.next(true, 0, c); i >= 0; i = used.next(true, i + 1, c)) {
            double pi = pri[i];
            if (pi < priMin) {
                priMin = pi;
                priMinIndex = i;
            }
        }
    }

    public int size() { return size; }
    public boolean isEmpty() { return size == 0; }
    public boolean contains(X item) { return itemToIndex.containsKey(item); }

    public List<X> toList() {
        Lst<X> l = new Lst<>(size);
        forEach(l::addFast);
        return l;
    }

    public X removeLast() {
        var x = lowestPriorityItem();
        remove(x);
        return x;
    }

    @Override
    public void close() {
        clear();
    }
}