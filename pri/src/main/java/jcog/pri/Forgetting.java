package jcog.pri;

import jcog.Is;
import jcog.Util;
import jcog.pri.bag.Bag;
import jcog.pri.op.PriAdd;
import jcog.pri.op.PriMult;
import jcog.random.RandomBits;
import jcog.random.XoRoShiRo128PlusRandom;
import org.jetbrains.annotations.Nullable;

import java.util.function.Consumer;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;
import static jcog.Util.*;
import static jcog.pri.Prioritized.EPSILON;

@Is("Forgetting_curve") public class Forgetting {



    public static @Nullable <Y> Consumer<Y> forget(Bag b, float strength) {
        return Forgetting.SimpleForgetMultiply.forgetMultiply(b, strength);

        //return Forgetting.SimpleForgetMultiplySigmoid.forget(b, strength);

        //return Forgetting.SimpleForgetSubtract.forgetSubtract(b, strength);

        //return Forgetting.InfoForgetting.forgetMultiply(b, strength);

        //return Forgetting.FieldForgetting.forget(b, strength);

    }


    private static double idealExcess(double m, int n) {
        return Math.max(idealLimitRate * (m - (n * idealMeanPri)), 0);
    }

    public static final double idealMeanPri =
        //1/2.0  //LINEAR DISTRIBUTION (definite Integral of f=x on 0..1)
        1/3.0  //X^2    DISTRIBUTION (definite Integral of f=x^2 on 0..1)
        //1/4f
        //1/8f
        ;

    public static final double idealLimitRate =
        //0; //DISABLED
        //1;
        0.5f;
        //0.1f;
        //0.01f;

    private static final boolean idealLimit = idealLimitRate > 0;

    /**
     * Implements forgetting based on instantaneous system properties.
     * 1. Conservation of total priority mass
     * 2. Pressure-based decay
     * 3. Homeostatic equilibrium
     *
     * Multiplicative decay preserves the relative ranking of items, with higher-priority items being less affected.
     */
    public enum SimpleForgetMultiply { ;

//        @Deprecated private static final boolean multMethod1Or2 = false;

        @Nullable
        public static <Y extends Prioritizable> PriMult<Y> forgetMultiply(Bag<?, Y> b, float temperature) {
            assertFinite(temperature);
            var s = b.size();
            if (s > 0) {
                var c = b.capacity();
                if (c > 0) {
                    double m = b.mass();
                    if (m > EPSILON) {

                        var pressure = b.depressurizePct(1);
                        var excess = temperature * pressure;
                        if (idealLimit) excess += idealExcess(m, s);

                        if (excess > EPSILON) {
                            var factor = (float)(//!multMethod1Or2 ?
                                /* asymptotic: */ m / (excess + m)
                                // /* linear: */ Util.max(1 - excess / m, 0) :
                            );
                            if (factor < 1 - EPSILON)
                                return new PriMult<>(factor);
                        }

                        b.pressurize(pressure); //return unused pressure
                    }
                }
            }
            return null;
        }

    }

    /**
     * decay_factor=1−(base_k+sigmoid((current_mass−ideal_mass)×scaling_factor))×temperature
     *   base_k: A constant representing the base decay rate.
     *   sigmoid(x): A sigmoid function to smoothly map the mass difference to the adjustment term.
     *   scaling_factor: Determines the sensitivity of the sigmoid function to the mass difference
     *   temperature: A parameter modulating the overall decay rate.
     */
    public static class SimpleForgetMultiplySigmoid {

        @Deprecated private static final boolean multMethod1Or2 = false;

        @Nullable
        public static <Y extends Prioritizable> PriMult<Y> forget(Bag<?, Y> b, float temperature) {
            assertFinite(temperature);
            var s = b.size();
            if (s > 0) {
                var c = b.capacity();
                if (c > 0) {
                    double m = b.mass();
                    if (m > EPSILON) {

                        var pressure = b.depressurizePct(1);

                        float base_k = 0;
                        float scalingFactor = pressure/2;
                        float factor = (float)(
                            1 - (base_k + sigmoid(m/c - idealMeanPri) * scalingFactor) * temperature
                        );

                        if (factor < 1 - EPSILON)
                            return new PriMult<>(factor);

                        b.pressurize(pressure); //return unused pressure
                    }
                }
            }
            return null;
        }

    }
    public static class SimpleForgetSubtract {


        @Nullable
        public static <Y extends Prioritizable> PriAdd<Y> forgetSubtract(Bag<?, Y> b, float temperature) {
            assertFinite(temperature);

            var s = b.size();
            if (s > 0) {
                var c = b.capacity();
                if (c > 0) {
                    double m = b.mass();
                    if (m > EPSILON) {

                        float pressure = b.depressurizePct(1);

                        double excess = ((double)temperature) * pressure;
                        if (idealLimit) excess += idealExcess(m, s);

                        var excessEach = (float) (excess / s);
                        if (excessEach > EPSILON)
                            return new PriAdd<>(-excessEach);

                        b.pressurize(pressure); //return unused pressure

                    }

                }
            }

            return null;
        }


//        /**
//         * allows strength values >1 for active forgetting
//         */
//        public static <X extends Prioritizable> Consumer<X> forgetSubtractSustain(Bag<?, X> b, float strength) {
//            assertValidForgetStrength(strength);
//            var s = b.size();
//            if (s <= 0) return null;
//
//            var p = forgetSubtract(b, Util.min(1, strength));
//            if (p == null) {
//                if (strength <= 1)
//                    return null;
//                p = new PriAdd<>(0);
//            }
//            if (strength > 1) {
//                double priMean = b.mass() / s;
//                p.x -= priMean * (strength - 1) * idealMeanPri;
//                //p.x -= priMean * (strength-1);
//            }
//
//            return p.x > -EPSILON ? null : p;
//        }
//
//        public static <X extends Prioritizable> Consumer<X> forgetMultiplySustain(Bag<?, X> b, float strength) {
//            assertValidForgetStrength(strength);
//            return forgetMultiply(b, strength);
//        }
//
//        public static void assertValidForgetStrength(float strength) {
//            if (strength < 0 || strength > 2)
//                throw new UnsupportedOperationException();
//        }
    }


    /**
     * Implements priority forgetting using field theory principles.
     * Priorities behave as charges in a field with quantum properties,
     * creating natural forgetting through field interactions.
     */
    public enum FieldForgetting { ;

        private static final double EPSILON = 1e-10;
        private static final double φ = (1 + sqrt(5)) / 2;  // Golden ratio

        /**
         * Field configuration for the cognitive space.
         * - entropyWeight: Balance between field and entropy effects [0,1]
         * - fieldStrength: Base interaction strength
         * - quantization: Minimum distinguishable priority difference
         */
        public record FieldConfig(
                double entropyWeight,
                double fieldStrength,
                double quantization
        ) {
            public FieldConfig {
                if (entropyWeight < 0 || entropyWeight > 1 ||
                        fieldStrength <= 0 || quantization <= 0) {
                    throw new IllegalArgumentException("Invalid field parameters");
                }
            }

        }

        public static final FieldConfig DEFAULT = new FieldConfig(0.5, 1.0, 0.01);

        /**
         * Creates a forgetting operation based on field interactions.
         * @param temperature Controls overall decay rate
         * @return Priority adjustment function or null if no adjustment needed
         */
        public static <X extends Prioritizable> Consumer<X> forget(Bag<?,X> b, float temperature/*, FieldConfig cfg*/) {

            var cfg = DEFAULT;

            if (temperature < 0 || !Double.isFinite(temperature))
                throw new IllegalArgumentException("Invalid temperature");

            var s = b.size();
            if (s == 0) return null;

            double mass = b.mass();
            if (mass <= EPSILON) return null;

            // Field metrics
            var density = mass / s;
            double pressure = b.pressure();

            // Field potential combining local density and global pressure
            var fieldPotential = sqrt(
                    pressure * density *
                            mass * cfg.fieldStrength / pow(s, φ)
            );

            // Entropy factor using q-exponential
            var entropyFactor = qExp(1 - density/mass, 1 + temperature);

            var entropyWeight = cfg.entropyWeight;

            // Combined decay strength
            var decayStrength = temperature * (
                    (1 - entropyWeight) * fieldPotential +
                            entropyWeight * entropyFactor
            );

            return decayStrength <= EPSILON ? null :
                decay((float) decayStrength, cfg);
        }

        /**
         * Creates the priority decay function based on field strength
         */
        private static <X extends Prioritizable> Consumer<X> decay(float strength, FieldConfig cfg) {

            var rng = new RandomBits(new XoRoShiRo128PlusRandom());
            return x -> {
                double pri = x.pri();

                float nextPri;

                // Quantum tunneling for small priorities
                if (pri < cfg.quantization && rng.nextBooleanFast8(strength)) {
                    nextPri = 0;
                } else {

                    // Field-based priority update with quantization
                    var phase = pri * φ;
                    var fieldEffect = strength * Math.sin(phase);
                    var continuous = pri - fieldEffect;

                    // Quantum effects near quantization boundaries
                    var quantized = Util.round(continuous, cfg.quantization);

                    // Interpolate based on field strength
                    nextPri = (float) Math.max(0, lerp(strength, continuous, quantized));
                }
                x.pri(nextPri);
            };
        }

        /**
         * Calculates q-exponential for non-extensive entropy
         */
        private static double qExp(double x, double q) {
            if (Math.abs(q - 1) < EPSILON) return Math.exp(x);
            var base = 1 + (1 - q) * x;
            return base > 0 ? pow(base, 1/(1-q)) : 0;
        }

    }

    /** Information-theoretic forgetting mechanism based on entropy maximization principles
     *
     *    Combines physical constraints (bag capacity) with information-theoretic principles (entropy) to create a self-regulating system that:
     *
     *    Maintains optimal cognitive load
     *    Fairly distributes resources
     *    Preserves important priorities
     *    Automatically adapts to system state
     *
     *    Homeostasis:
     *      System maintains balance around IDEAL_ENTROPY
     *      Automatic adjustment based on current state
     *      System increases forgetting when entropy deviates from ideal
     *    Scale invariant (works with any priority range)
     *    Numerically stable (handles small values)
     *    Continuous and smooth transitions     *
     *
     *    a) Multiplicative
     *      Asymptotic decay that preserves relative priorities
     *      Entropy-weighted to maintain distribution balance
     *      More stable for long-term memory simulation
     *    b) Subtractive:
     *      Linear decay that reduces absolute priorities
     *      Distributes forgetting pressure evenly
     *      Better for immediate resource management

     * */
    public enum InfoForgetting {
        ;

        /** ln(3) == ~1.098: theoretical optimum for cognitive load */
        private static final double ENTROPY_IDEAL = Math.log(3);

        private static final double EPSILON = Prioritized.EPSILON;

        public static <Y extends Prioritizable> PriMult<Y> forgetMultiply(Bag<?,Y> b, float temperature) {
            var s = b.size();
            var m = b.mass();
            if (!isValid(temperature, s, m)) return null;

            var H = entropy(b, m);
            var excess = temperature * pressure(b, H, s);
            if (idealLimit) excess += idealExcess(m, s /*b.capacity()*/);

            if (excess <= EPSILON) return null;

            var factor = (float)(m / (excess + m) * sqrt(H/Math.log(s)));
            return (factor < 1 - EPSILON) ? new PriMult<>(factor) : null;
        }

        private static <Y extends Prioritizable> double pressure(Bag<?, Y> b, double H, int n) {
            return b.depressurizePct(1) *
                    (1 + Math.max(0, (ENTROPY_IDEAL - H)));
                    //(1 + Util.max(0, (ENTROPY_IDEAL - H) / Math.log(n)));
                   //(1 + Util.max(0, IDEAL_ENTROPY - H / Math.log(n)));
        }

        private static <Y extends Prioritizable> boolean isValid(float temperature, int n, float mass) {
            return temperature > 0 && n > 0 && mass > EPSILON;
        }

        private static <Y extends Prioritizable> double entropy(Bag<?,Y> b, double mass) {
            double H = 0;
            for (var x : b) {
                var p = x.priElseZero() / mass;
                if (p > EPSILON) H -= p * Math.log(p);
            }
            return H;
        }
//        private static <Y extends Prioritizable> double entropyB(Bag<?,Y> b, double mass) {
//            double BOLTZMANN = 1;  // Normalized constant
//            double Z = 0;  // Partition function
//            double E = 0;  // Energy
//            double T = b.pressure();  // Temperature from pressure
//
//            for (var p : b) {
//                double e = -Math.log(p.priElseZero() + EPSILON);  // Energy level
//                var dz = Math.exp(-e / (BOLTZMANN * T));
//                Z += dz;
//                E += e * dz;
//            }
//            return Math.log(Z) + E / (T * BOLTZMANN);
//        }

    }


}
